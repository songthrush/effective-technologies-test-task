-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: testtask
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.10.2
--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `author_id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (1,'Longfellow'),(2,'Poe'),(3,'Cooper');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `book_id` int(8) NOT NULL AUTO_INCREMENT,
  `year` int(4) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (1,1826,'The Last of the Mohicans'),(2,1841,'The Deerslayer'),(3,1845,'Raven'),(4,1838,'Predicament'),(5,1848,'An Enigma'),(6,1855,'Hiawatha'),(7,1849,'Own Poe Cooper');
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books_authors`
--

DROP TABLE IF EXISTS `books_authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books_authors` (
  `book_id` int(8) NOT NULL,
  `author_id` int(8) NOT NULL,
  PRIMARY KEY (`book_id`,`author_id`),
  KEY `author_id` (`author_id`),
  CONSTRAINT `books_authors_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`),
  CONSTRAINT `books_authors_ibfk_2` FOREIGN KEY (`author_id`) REFERENCES `authors` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books_authors`
--

LOCK TABLES `books_authors` WRITE;
/*!40000 ALTER TABLE `books_authors` DISABLE KEYS */;
INSERT INTO `books_authors` VALUES (6,1),(3,2),(4,2),(5,2),(7,2),(1,3),(2,3),(7,3);
/*!40000 ALTER TABLE `books_authors` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

-- Dump completed on 2019-02-23  1:20:03

insert into authors values
	(null, 'Longfellow'),
	(null, 'Poe'),
	(null, 'Cooper');
	
insert into books values
	(null, 1826, 'The Last of the Mohicans'),
	(null, 1841, 'The Deerslayer'),
	(null, 1845, 'Raven'),
	(null, 1838, 'Predicament'),
	(null, 1848, 'An Enigma'),
	(null, 1855, 'Hiawatha'),
	(null, 1849, 'Own Poe Cooper');
	
insert into books_authors values
	(7, 2),
	(7, 3),
	(1, 3),
	(2, 3),
	(3, 2),
	(4, 2),
	(5, 2),
	(6, 1);
