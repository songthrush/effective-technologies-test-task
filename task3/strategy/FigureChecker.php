<?php

require_once "classes/Figure.php";

class FigureChecker
{
    public function getSquare($figure)
    {
        try {
            return $figure->getSquare();
        }
        catch (Error $e) {
            return 0;
        }
    }
}