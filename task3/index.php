<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once 'classes/Circle.php';
require_once 'classes/Rectangle.php';
require_once 'classes/Pyramide.php';
require_once 'classes/Triangle.php';
require_once 'classes/Figure.php';

require_once 'strategy/FigureChecker.php';
require_once 'factory/FigureFactory.php';

$figures = [];
$amount = rand(4, 10);
$factory = new FigureFactory();

for ($i = 0; $i < $amount; $i++) {
    $figure = $factory->getFigure(rand(0, 3));
    array_push($figures, $figure);
}

$file = fopen("file.json", "w+") or die("File reading error");
fwrite($file, serialize($figures));
fseek($file, 0);
$figures_new = unserialize(fread($file, filesize("file.json")));
fclose($file);

function sortBySquare($a, $b)
{ 
    $sq1 = $a instanceof Figure ? $a->getSquare() : 0;
    $sq2 = $b instanceof Figure ? $b->getSquare() : 0;

    if ($sq1 == $sq2) {
        return 0;
    }
    return ($sq1 > $sq2) ? -1 : 1;
}

usort($figures_new, "sortBySquare");

$figureChecker = new FigureChecker();

for ($i = 0; $i < count($figures_new); $i++) {
    print_r($figures_new[$i]);
    echo " | ";
    print_r($figureChecker->getSquare($figures_new[$i]));
    echo "<br>";
}
?>
