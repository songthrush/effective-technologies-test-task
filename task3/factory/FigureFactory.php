<?php
require_once 'classes/Circle.php';
require_once 'classes/Rectangle.php';
require_once 'classes/Pyramide.php';
require_once 'classes/Triangle.php';

class FigureFactory
{
    private $min = 1;
    private $max = 20;

    public function getFigure($figure_code)
    {
        switch ($figure_code) {
            case 0:
                $figure = new Circle(rand($this->min, $this->max));
                break;
            case 1:
                $figure = new Rectangle(rand($this->min, $this->max), rand($this->min, $this->max));
                break;
            case 2:
                $figure = new Triangle(rand($this->min, $this->max));
                break;
            case 3:
                $base = rand($this->min, $this->max);
                $edge = rand($this->min, $this->max);
                //чтобы пирамида существовала
                if ($base / 2 >= $edge) {
                    $edge = $base  / 2 + 1;
                }
                $figure = new Pyramide($base, $edge);
                break;
        }
        return $figure;
    }
}
