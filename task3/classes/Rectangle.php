<?php
require_once "Figure.php";

class Rectangle implements Figure, JsonSerializable
{
    public $a;
    public $b;

    function __construct($a, $b)
    {
        $this->a = $a;
        $this->b = $b;
    }

    public function getSquare()
    {
        return $this->a * $this->b;
    }

    public function jsonSerialize()
    {
        return [
            'a' => $this->a,
            'b' => $this->b
        ];
    }
}
?>
