<?php
require_once "Figure.php";

class Circle implements Figure, JsonSerializable
{
    public $radius;

    function __construct($r)
    {
        $this->radius = $r;
    }

    public function getSquare()
    {
        return pow($this->radius, 2) * 3.14;
    }

    public function jsonSerialize()
    {
        return [
            'radius' => $this->radius
        ];
    }
}
