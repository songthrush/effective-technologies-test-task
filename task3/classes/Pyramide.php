<?php
require_once "Figure.php";

class Pyramide implements Figure, JsonSerializable
{
    public $base;
    public $edge;

    function __construct($base, $edge)
    {
        $this->base = $base;
        $this->edge = $edge;
    }

    private function getTriangleSquare($base, $edge)
    {
        $height = sqrt(pow($edge, 2) - pow(($base / 2), 2));
        $square = 0.5 * $base * $height;
        return $square;
    }

    public function getSquare()
    {
        $square = 4 * $this->getTriangleSquare($this->base, $this->edge) + pow($this->base, 2);
        $square = round($square, 3);
        return $square;
    }

    public function jsonSerialize()
    {
        return [
            'base' => $this->base,
            'edge' => $this->edge
        ];
    }
}
?>
