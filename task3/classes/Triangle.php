<?php
//for testing errors
class Triangle implements JsonSerializable
{
	public $radius;

    function __construct($r)
    {
        $this->radius = $r;
    }

    public function jsonSerialize()
    {
        return [
            'radius' => $this->radius
        ];
    }
}
?>
